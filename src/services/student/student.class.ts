import { Db } from 'mongodb';
import { Service, MongoDBServiceOptions } from 'feathers-mongodb';
import { Application } from '../../declarations';
import { Params, Id } from '@feathersjs/feathers';


interface StudentData {
  name: string;
  father_name: string;
  email: string;
  roll_no: number;
  gr_no: number;
  gender: string;
  status: boolean;
  mobile?: number;
  address: string;
}

export class Student extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongoDBServiceOptions>, app: Application) {
    super(options);

    const client: Promise<Db> = app.get('mongoClient');

    client.then(db => {
      this.Model = db.collection('student');
    });
  }

  // Add New Student

  addStudent(data: StudentData) {
    const { name, father_name, email, roll_no, gr_no, gender, status, mobile, address } = data;
    const StudentData = { name, father_name, email, roll_no, gr_no, gender, status, mobile, address };

    return super.create(StudentData);
  }



  // Delete Student By ID

  deleteStudent(id: Id) {
    return super.remove(id)
  }


  // Get Student By ID

  getStudentById(id:Id){
    return super.get(id)
  }

  //Update Student By ID


  updateStudent(id:Id,data: StudentData){
    const { name, father_name, email, roll_no, gr_no, gender, status, mobile, address } = data;
    const StudentData = { name, father_name, email, roll_no, gr_no, gender, status, mobile, address };
    return super.update(id,StudentData)
  }

  //Get All Student List
  getStudent(){
    return super.get
  }

};





