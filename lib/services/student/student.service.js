"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const student_class_1 = require("./student.class");
const student_hooks_1 = __importDefault(require("./student.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/student', new student_class_1.Student(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('student');
    service.hooks(student_hooks_1.default);
}
exports.default = default_1;
