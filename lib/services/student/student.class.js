"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Student = void 0;
const feathers_mongodb_1 = require("feathers-mongodb");
class Student extends feathers_mongodb_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
        const client = app.get('mongoClient');
        client.then(db => {
            this.Model = db.collection('student');
        });
    }
    // Add New Student
    addStudent(data) {
        const { name, father_name, email, roll_no, gr_no, gender, status, mobile, address } = data;
        const StudentData = { name, father_name, email, roll_no, gr_no, gender, status, mobile, address };
        return super.create(StudentData);
    }
    // Delete Student By ID
    deleteStudent(id) {
        return super.remove(id);
    }
    // Get Student By ID
    getStudentById(id) {
        return super.get(id);
    }
    //Update Student By ID
    updateStudent(id, data) {
        const { name, father_name, email, roll_no, gr_no, gender, status, mobile, address } = data;
        const StudentData = { name, father_name, email, roll_no, gr_no, gender, status, mobile, address };
        return super.update(id, StudentData);
    }
    getStudent() {
        return super.get;
    }
}
exports.Student = Student;
;
